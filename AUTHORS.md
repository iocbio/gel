# Authors and Contributors of IOCBIO Gel software

The base of IOCBIO Gel was developed as a student project for the Laboratory of
Systems Biology, Department of Cybernetics, School of Science, Tallinn
University of Technology. Later, it was developed further by the laboratory for
internal use and public release.

The authors and maintainers are listed below alphabetically.

## Authors

- Lauri Kask <lakask@taltech.ee>
- Jaak Kütt <jakutt@taltech.ee>
- Martin Laasmaa <martin@sysbio.ioc.ee>
- Georg Margus <gemarg@taltech.ee>
- Marko Vendelin <markov@sysbio.ioc.ee>

## Maintainers

- Martin Laasmaa <martin@sysbio.ioc.ee>
- Marko Vendelin <markov@sysbio.ioc.ee>
